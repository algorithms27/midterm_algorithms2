/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.midterm_algorithms2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author natthawadee
 */
public class Midterm_Algorithms2 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println(" Please Enter any Number you want : ");
        int[] num = new int[kb.nextInt()];

        for (int i = 0; i < num.length; i++) {
            num[i] = kb.nextInt();
        }
        ReverseArray(num);
        System.out.printf(" Reverse of entered is = " + Arrays.toString(num));

    }

    private static void ReverseArray(int[] num) {
        for (int i = 0; i < num.length / 2; i++) {
            int temp = num[i];
            num[i] = num[num.length - i - 1];
            num[num.length - i - 1] = temp;
        }
    }
}
